﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BlackBoard : MonoBehaviour
{
    [HideInInspector]
    public Transform goldDropOffPoint;
    [HideInInspector]
    public Transform woodDropOffPoint;
    private Transform tree;
    private Transform mine;
    private GameObject astar;
    private float speedSlider;

    private static float treeDistance = 0.0f;
    private static float mineDistance = 0.0f;

    [Flags]
    public enum Building
    {
        BlackSmith    = 1,      //0000 0000 0001
        House         = 2,      //0000 0000 0010  
        Farm          = 4,      //0000 0000 0100
        WizardTower   = 8,      //0000 0000 1000
        Cathedral     = 16,     //0000 0001 0000
        LumberMill    = 32,     //0000 0010 0000
        GoldMine      = 64,     //0000 0100 0000
        Stable        = 128,    //0000 1000 0000
        SiegeWorkshop = 256,    //0001 0000 0000
        Barracks      = 512,    //0010 0000 0000
    }


    [HideInInspector]
    public static Building allBuildings = Building.GoldMine;
    [HideInInspector]
    public static int gold = 0;
    [HideInInspector]
    public static int wood = 40;
    [HideInInspector]
    public static int currentFood = 4;
    [HideInInspector]
    public static int maxFood = 4;
    [HideInInspector]
    public static float currGoldRate = 0.0f;
    [HideInInspector]
    public static float currWoodRate = 0.0f;
    [HideInInspector]
    public static int maxHaul = 10;
    [HideInInspector]
    public static int numGoldWorkers = 2;
    [HideInInspector]
    public static int numWoodWorkers = 2;
    [HideInInspector]
    public static int numWorkersBuilding = 0;
    [HideInInspector]
    public static int workerSpeed = 15;

    void Start()
    {
        speedSlider      = GameObject.Find("Slider").GetComponent<Slider>().value;
        goldDropOffPoint = GameObject.Find("TownHallCenter").GetComponent<Transform>();
        woodDropOffPoint = goldDropOffPoint;
        mine  = GameObject.Find("MineCenter").GetComponent<Transform>();
        astar = GameObject.Find("A*");

        // initial distance calculations to be used in gold and wood collection 
        GameObject closestTree  = FindNearestObjectByTag.GetNearestObjectByTag(goldDropOffPoint.position, "Tree");
        mineDistance = Vector3.Distance(goldDropOffPoint.position, mine.position);
        treeDistance = Vector3.Distance(woodDropOffPoint.position, closestTree.GetComponent<Transform>().position);

        UpdateCurrGold();
        UpdateCurrWood();
    }

    void Update()
    {
        speedSlider = GameObject.Find("Slider").GetComponent<Slider>().value;
        workerSpeed = (int)Mathf.Ceil(speedSlider);
        OnStateChange();

        if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public static void AddBuilding(Building b)
    {
        allBuildings |= b;
    }

    public static void RemoveBuilding(Building b)
    {
        allBuildings = allBuildings & (~b);
    }

    public static bool HasBuilding(Building b)
    {
        return (allBuildings & b) == b;
    }

    public static void UpdateCurrGold()
    {
        currGoldRate = (workerSpeed / (mineDistance * 2)) * (maxHaul * numGoldWorkers);
        currGoldRate = Round(currGoldRate, 2);
    }

    public static void UpdateCurrWood()
    {
        currWoodRate = (workerSpeed / (treeDistance * 2)) * (maxHaul * numWoodWorkers);
        currWoodRate = Round(currWoodRate, 2);
    }

    public static float Round(float value, int digits)
    {
        float mult = Mathf.Pow(10.0f, (float)digits);
        return Mathf.Round(value * mult) / mult;
    }

    public static void OnStateChange()
    {
        int gWorkers = 0;
        int wWorkers = 0;

        GameObject peasants = GameObject.Find("Peasants");
        WorkerLogic[] peasantChildren = peasants.GetComponentsInChildren<WorkerLogic>();
        for (int j = 0; j < peasantChildren.Length; ++j)
        {
            if (peasantChildren[j].state == WorkerLogic.WORKER_STATE.WORKER_BAG)
                gWorkers++;
            if (peasantChildren[j].state == WorkerLogic.WORKER_STATE.WORKER_WOOD)
                wWorkers++;               
        }
        numGoldWorkers = gWorkers;
        numWoodWorkers = wWorkers;

        UpdateCurrGold();
        UpdateCurrWood();
    }
}
