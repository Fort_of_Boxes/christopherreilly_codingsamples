﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public Queue<BuildPlan> masterPlan;
    public static string masterPlanAsText; 

    void Start()
    {
        masterPlan = new Queue<BuildPlan>();   
    }

    public void BuildPlan(string plan)
    {
        AddBuildPlan(plan);
    }

    public void AddBuildPlan(string plan)
    {
        BuildPlan mPlan = Instantiate(Resources.Load(plan, typeof(BuildPlan))) as BuildPlan;
        masterPlanAsText = mPlan.ObjectName;
        masterPlan.Enqueue(mPlan);
    }

    void ProcessBuildQueue()
    {
        if (masterPlan.Count == 0)
            return;

        PlanProcessor.ClearPlans();
        // dequeue a build requests and send it to the processor
        PlanProcessor.ProcessPlan(masterPlan.Dequeue());
    }

    void Update()
    {
        ProcessBuildQueue();
    }

}
