﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class BuildPlan : ScriptableObject
{
    public string ObjectName = "BuildPlan";
    public Plan plan;
}
    