﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

    //public delegate void BuildingRequest();
    //public static event BuildingRequest OnBuildingRequest;
    


    public delegate void BuildingConstructionStart();
    public static event BuildingConstructionStart OnBuildingStart;

    public delegate void BuildRequest(Plan p, WorkerLogic worker);
    public static event BuildRequest OnBuildRequest;

    public delegate void BuildLocationFound(Vector3 pos, Plan p);
    public static event BuildLocationFound OnLocationFound;

    public delegate void UpdateGrid();
    public static event UpdateGrid OnUpdateGrid;

    public static void BuildingLocationFound(Vector3 pos, Plan p)
    {
        Debug.Log("BuildingLocationFound Event:");
        if (OnLocationFound != null) // error if no subscribers
            OnLocationFound(pos, p);
    }


    public static void WorkerBuild(Plan p, WorkerLogic worker)
    {
        Debug.Log("WorkerBuild Event:");
        if (OnBuildRequest != null) // error if no subscribers
            OnBuildRequest(p, worker);
    }

    public static void StartBuilding()
    {
        Debug.Log("StartBuilding Event:");
        if(OnBuildingStart != null) // error if no subscribers
            OnBuildingStart();
    }

    public static void UpdatedGrid()
    {
        Debug.Log("UpdatedGrid Event:");

        if (OnUpdateGrid != null)
            OnUpdateGrid();
    }



}
