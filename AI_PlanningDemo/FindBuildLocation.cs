﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TO SELECT WHERE TO BUILD A Bulding
public class FindBuildLocation : MonoBehaviour
{
    struct LocationInfo
    {
        public Node location;
        public float opennessScore; // higher equals better on a scale from 5 to zero
    };

    Grid grid;
    BlackBoard blackBoard;

    void Awake()
    {
        grid = GameObject.Find("A*").GetComponent<Grid>();
        blackBoard = GameObject.Find("BlackBoard").GetComponent<BlackBoard>();
    }

    void OnEnable()
    {
        EventManager.OnBuildRequest += LocationForBuilding;

    }

    void OnDisable()
    {
        EventManager.OnBuildRequest -= LocationForBuilding;
    }

    // Use this for initialization
    void LocationForBuilding(Plan p, WorkerLogic worker)
    {
        Debug.Log("FindingLocation!!:)");
        LocationInfo locInfo = new LocationInfo();
        // Get Unit position, check the sphere of the object against that of the ground, if it is able to go there then place it there..
        Transform trans = worker.transform;



        if (p.inputResource != "") // Building depends on a resource so place near the resource
        {
            GameObject nearest = FindNearestObjectByTag.GetNearestObjectByTag(trans.position, p.inputResource);

            locInfo.opennessScore = 0; // effectively set to not open
            while (locInfo.opennessScore == 0) // try it until we get a good spot
            {
                int currItteration = 0;
                for (int i = 2; i < 5; ++i) // this is less worried about openness compared to nearness to tree
                {
                    Vector3 samplePos = Random.insideUnitCircle;

                    float randScalarX = Random.Range(p.buildingRadius * 8, p.buildingRadius * 10);
                    float randScalarY = 0;
                    float randScalarZ = Random.Range(p.buildingRadius * 8, p.buildingRadius * 10);
                    Vector3 rand = new Vector3(randScalarX, randScalarY, randScalarZ);
                    rand += nearest.transform.position;
                    
                    samplePos = rand;
                    LocationInfo info = FindOpenLocationNearPoint(samplePos + p.buildingOffset, p.buildingRadius);
                    if (info.opennessScore > locInfo.opennessScore) // found a spot that is more open
                    {
                        locInfo = info;
                    }
                }
                currItteration++;
            }
        }
        else // Just trying to find a good spot for a building
        {

            locInfo.opennessScore = 0; // effectively set to not open
            while (locInfo.opennessScore == 0) // try it until we get a good spot
            {
                int currItteration = 0;
                for (int i = 2; i < 10; ++i) // sample ten times to get a good open position
                {
                    Vector3 samplePos = Random.insideUnitCircle * (p.buildingRadius * i);
                    samplePos += trans.position;

                    LocationInfo info = FindOpenLocationNearPoint(samplePos, p.buildingRadius);
                    if (info.opennessScore > locInfo.opennessScore) // found a spot that is more open
                    {
                        locInfo = info;
                    }
                }
                currItteration++;
            }
        }

        locInfo.location.worldPosition.y = trans.position.y;
        worker.buildLocation = GetNearestPointToBuilding(trans.position, locInfo.location.worldPosition, p.buildingRadius, 15);

        worker.buildPathFound = true;

        //Send BuildLocationFoundEvent
        EventManager.BuildingLocationFound(locInfo.location.worldPosition, p);

        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.AddComponent<DestroyOverTime>();
        sphere.transform.position = worker.buildLocation;

    }

    LocationInfo FindOpenLocationNearPoint(Vector3 pos, float buildingRadius)
    {

        int[] offsetXCorner = new int[] { 0, 1, 0, -1, 1, 1, -1, -1, }; // optimization for neighbors (corners last)
        int[] offsetYCorner = new int[] { -1, 0, 1, 0, -1, 1, 1, -1, };
        int[] activeDirection = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, }; // disable the checks for a bit if they arent good

        LocationInfo locInfo = new LocationInfo();
        locInfo.opennessScore = 0;
        Node node = grid.NodeFromWorldPos(pos);
        for (int offset = (int)Mathf.Ceil(buildingRadius); offset < 50; offset += 5) //guessing the values for now 
        {
            for (int i = 0; i < 8; i++)
            {
                int x = node.gridX + (i * offset);
                int y = node.gridY + (i * offset);
                if (!(Physics.CheckSphere(grid.grid[x, y].worldPosition, buildingRadius, grid.unwalkableMask))) // if it does not collide with unwalkable mask
                {
                    // Find Positions of Points if(!)
                    locInfo.location = grid.grid[x, y];
                    for (int k = 10; k > 2; --k) // determine openness of position
                    {
                        if (!(Physics.CheckSphere(grid.grid[x, y].worldPosition, buildingRadius * k, grid.unwalkableMask)))
                        {
                            locInfo.opennessScore = k;
                            return locInfo;
                        }
                    }
                }
            }
        }
        return locInfo;
    }

    public Vector3 GetNearestPointToBuilding(Vector3 pos, Vector3 buildingPos, float radius, float epsilon) // have the peasent go to a point near the building
    {
        Vector3 dir = pos - buildingPos;
        dir.Normalize();
        dir *= (radius * (3));
        dir.y = pos.y;

        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.gameObject.AddComponent<DestroyOverTime>();
        sphere.transform.position = (buildingPos + new Vector3(dir.x, 0, dir.z));

        return (buildingPos + new Vector3(dir.x, 0, dir.z));
    }
}