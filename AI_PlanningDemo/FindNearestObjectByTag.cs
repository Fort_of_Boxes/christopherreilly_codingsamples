﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindNearestObjectByTag : MonoBehaviour
{
    static FindNearestObjectByTag instance;
    void Start()
    {
        instance = this;
    }

   public static GameObject GetNearestObjectByTag(Vector3 pos, string tagName)
    {
        // Node  node = grid.NodeFromWorldPos(pos);
        // Either Grow circle outwards
        
        int index = -1;
        float distanceMax = GameObject.Find("A*").GetComponent<Grid>().MaxGridSize;

        GameObject[] objects = GameObject.FindGameObjectsWithTag(tagName);
        for(int i = 0; i < objects.Length;++i)
        {
            float newDist = Vector3.Distance(pos, objects[i].transform.position);
            if (newDist < distanceMax)
            {
                index = i;
                distanceMax = newDist;
            }
        }
        if (index > -1)
            return objects[index];
        else
            return null;

    }
}
