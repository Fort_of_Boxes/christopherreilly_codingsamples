﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public Transform Player;
    public LayerMask unwalkableMask;
    //Resources to collect
    public LayerMask woodMask;
    public LayerMask goldMask;
    
    public Vector2 gridWorldSize;
    public float nodeRadius;
    [HideInInspector]
    public Node[,] grid;
    [HideInInspector]
    List<int> xResources = new List<int>(); 
    [HideInInspector]
    List<int> yResources = new List<int>(); // to hold the indices of places that have a resource on them

    float nodeDiameter;
    [HideInInspector]
    public int gridSizeX, gridSizeY;
    void Awake()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }
    void OnEnable()
    {
        EventManager.OnBuildingStart += UpdateGrid;
    }

    void OnDisable()
    {
        EventManager.OnBuildingStart -= UpdateGrid;
    }

    public int MaxGridSize
    {
        get{
         return gridSizeX * gridSizeY;
        }
    }

    void CreateGrid()
    {

        grid = new Node[gridSizeX, gridSizeY];
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
        int woodProp = 0;
        int goldProp = 1;
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius,unwalkableMask));

                if ((Physics.CheckSphere(worldPoint, nodeRadius, woodMask)))
                {
                    woodProp = 1;
                    xResources.Add(x);
                    yResources.Add(y);
                }
                else
                    woodProp = 0;

                if ((Physics.CheckSphere(worldPoint, nodeRadius, goldMask)))
                {
                    goldProp = 1;
                    xResources.Add(x);
                    yResources.Add(y);
                }
                else
                    goldProp = 0;

                grid[x, y] = new Node(walkable, worldPoint,x ,y, goldProp, woodProp);
            }
        }
    }

    void UpdateGrid() // Update whether the grid spot is walkable when a building is constructed
    {
        for (int x = 0; x < gridSizeX; x++)
            for (int y = 0; y < gridSizeY; y++)
                grid[x, y].walkable = !(Physics.CheckSphere(grid[x, y].worldPosition, nodeRadius, unwalkableMask));

        EventManager.UpdatedGrid();
    }

    public Node NodeFromWorldPos(Vector3 worldPosition)
    {
        float percentX = Mathf.Clamp01((worldPosition.x + gridWorldSize.x/2) / gridWorldSize.x);
        float percentY = Mathf.Clamp01((worldPosition.z + gridWorldSize.y/2) / gridWorldSize.y);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
        return grid[x, y];

    }

    public List<Node> GetNeighbors(Node center)
    {
        List<Node> neighbors = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;
                int checkX = center.gridX + x;
                int checkY = center.gridY + y;
                if (checkX >= 0 && checkX < gridSizeX &&
                   checkY  >= 0 && checkY < gridSizeY)
                    neighbors.Add(grid[checkX, checkY]);
            }
        }
        return neighbors;
    }
}
