﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : IHeapItem<Node>
{
    public bool walkable;
    public Vector3 worldPosition;
    public int givenCost;
    public int heurCost;
    public int gridX;
    public int gridY;
    public int goldProp;
    public int woodProp;
    public int buildingProp;
    public Node parent;
    int heapIndex;

    public Node(bool ableToWalk, Vector3 worldPos, int xGridPos, int yGridPos, int goldPropagation, int woodPropagation)
    {
        walkable = ableToWalk;
        worldPosition = worldPos;
        gridX = xGridPos;
        gridY = yGridPos;
        goldProp = goldPropagation;
        woodProp = woodPropagation;
    }
    
    public int fCost //FixedCost for A*
    {
        get { return givenCost + heurCost; }
    }
    void IsWalkable(bool ableToWalk)
    {
        walkable = ableToWalk;
    }
    Vector3 GetPos()
    {
        return worldPosition;
    }

    public int HeapIndex
    {
        get{
            return heapIndex;
        }
        set        {
            heapIndex = value;
        }
    }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = fCost.CompareTo(nodeToCompare.fCost);
        if(compare == 0) // they are equal
        {
            compare = heurCost.CompareTo(nodeToCompare.heurCost); // returns one if its higher
        }
        return -compare; // we want the oppposite;
    }
}
