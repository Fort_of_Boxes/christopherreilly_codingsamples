﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;

public class Pathfinding : MonoBehaviour {
    PathRequestManager requestManager;
    Grid grid;

    void Awake()
    {
        requestManager = GetComponent<PathRequestManager>();
        grid = GetComponent<Grid>();
    }

    public void StartFindPath(Vector3 startPos, Vector3 targetPos)
    {
        StartCoroutine(FindPath(startPos, targetPos));
    }

    IEnumerator FindPath(Vector3 startPos, Vector3 targetPos)
    {

        Stopwatch sw = new Stopwatch();
        sw.Start();

        Vector3[] waypoints = new Vector3[0];
        bool pathSuccess = false;

        Node startNode = grid.NodeFromWorldPos(startPos);
        Node targetNode = grid.NodeFromWorldPos(targetPos);
        startNode.parent = startNode;


        if (startNode.walkable && targetNode.walkable)
        {
            Heap<Node> openSet = new Heap<Node>(grid.MaxGridSize);
            HashSet<Node> closedSet = new HashSet<Node>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    sw.Stop();
                   // print("Path found: " + sw.ElapsedMilliseconds + " ms");
                    pathSuccess = true;
                    break;
                }

                foreach (Node neighbour in grid.GetNeighbors(currentNode))
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    int newMovementCostToNeighbour = currentNode.givenCost + GetDistance(currentNode, neighbour);
                    if (newMovementCostToNeighbour < neighbour.givenCost || !openSet.Contains(neighbour))
                    {
                        neighbour.givenCost = newMovementCostToNeighbour;
                        neighbour.heurCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour))
                            openSet.Add(neighbour);
                        else
                            openSet.UpdateItem(neighbour);
                    }
                }
            }
        }
        yield return null;
        if (pathSuccess)
        {
            waypoints = RetracePath(startNode, targetNode);
        }
        requestManager.FinishedProcessingPath(waypoints, pathSuccess);

    }
    // IEnumerator FindPath (Vector3 startPos, Vector3 endPos) //actual A*
    // {
    //     Stopwatch sw = new Stopwatch();
    //     sw.Start();
    //     Vector3[] wayPoints = new Vector3[0];
    //     bool pathSuccess = false;
    //
    //     Node startNode = grid.NodeFromWorldPos(startPos);
    //     Node targetNode = grid.NodeFromWorldPos(endPos);
    //
    //     if(startNode.walkable && targetNode.walkable) // only if both spots aree walkable
    //     {
    //
    //         Heap<Node> openList = new Heap<Node>(grid.MaxGridSize);
    //         HashSet<Node> closedSet = new HashSet<Node>();
    //
    //         openList.Add(startNode);
    //         while(openList.Count > 0)
    //         {
    //             Node currNode = openList.RemoveFirst();
    //             closedSet.Add(currNode);
    //
    //             if (currNode == targetNode)
    //             {
    //                 sw.Stop();
    //                // print("PATH FOUND: " + sw.ElapsedMilliseconds + " ms");
    //                 pathSuccess = true;
    //                 break;
    //             }
    //             foreach (Node neighbor in grid.GetNeighbors(currNode))
    //             {
    //                 if (!neighbor.walkable || closedSet.Contains(neighbor))
    //                     continue;
    //
    //                 int newMovementCostToNeighbor = currNode.givenCost + GetDistance(currNode, neighbor);
    //                 if(newMovementCostToNeighbor < neighbor.givenCost || !openList.Contains(neighbor))
    //                 {
    //                     neighbor.givenCost = newMovementCostToNeighbor;
    //                     neighbor.heurCost = GetDistance(neighbor, targetNode);
    //                     neighbor.parent = currNode;
    //                     if (!openList.Contains(neighbor))
    //                         openList.Add(neighbor);
    //                     else
    //                         openList.UpdateItem(neighbor);
    //                 }
    //
    //             }
    //         }
    //     }
    //
    //     yield return null;
    //     if(pathSuccess)
    //     {
    //         wayPoints = RetracePath(startNode, targetNode);
    //     }
    //     requestManager.FinishedProcessingPath(wayPoints, pathSuccess);
    // }

    Vector3[] RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);
        return waypoints;

    }

    Vector3[] SimplifyPath(List<Node> path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 directionOld = Vector2.zero;

        for (int i = 1; i < path.Count; i++)
        {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            if (directionNew != directionOld)
            {
                waypoints.Add(path[i].worldPosition);
            }
            directionOld = directionNew;
        }
        return waypoints.ToArray();
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }
}
