﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Used to evaluate which resources should be collected and organize units for minimal time to reach goal */
public class PlanProcessor : MonoBehaviour
{
    private static Stack<BuildPlan> currentPlans;    // holds all currentPlans we need to execute for the mPlan
    private BuildPlan               currentPlan;     // the currentplans being executed
    private static bool             isProcessing;    // if we are processing a plan for the currentPlans stack
    public  static string           currentPlanName; // the name of the current plan being executed

    private bool         makingAssignment;            // whether or not we are assigning tasks for the current plan
    private static int   goldWorkersToBeAssigned;     // how many gold workers we need to assign for the current plan
    private static int   woodWorkersToBeAssigned;     // how many wood workers we need to assign for the current plan
    private static int   totalWorkersLeftToBeAssiged;
    private float        assignmentTimer = 0.0f;     // how often we will check to reassign workers during the current plan's execution
    private float        resetTime  = 30.0f;
    private float        resetSpeed = 4.0f;

    void Start()
    {
        isProcessing = false;
        makingAssignment = false;
        currentPlans = new Stack<BuildPlan>();
    }

    // clears out plans when new master request is received 
    public static void ClearPlans()
    {
        while (currentPlans.Count > 0)
            currentPlans.Pop();
        currentPlans.Clear();
        currentPlanName = "None";
        isProcessing = false;
    }

    // recursively adds all building requirements and their plans
    public static void ProcessPlan(BuildPlan plan)
    {
        currentPlans.Push(plan);

        // base case, there are no required buildings
        if (plan.plan.buildingsRequirements.Length == 0)
            return;

        // for all building requirements of this plan
        for (int i = 0; i < plan.plan.buildingsRequirements.Length; ++i)
        {
            // if we currently have the building, we dont need to add it 
            if (BlackBoard.HasBuilding(plan.plan.buildingsRequirements[i]))
                continue;
            else
            {
                // process the building requirement plan
                string planToAdd = plan.plan.buildingsRequirements[i].ToString();
                planToAdd += "Data";
                BuildPlan mPlan = Instantiate(Resources.Load(planToAdd, typeof(BuildPlan))) as BuildPlan;
                ProcessPlan(mPlan);
            }
        }
    }

    void Update()
    {
        // decrement assignmentTimer
        assignmentTimer -= Time.deltaTime * resetSpeed;

        // for all current plans in the stack, process them in order 
        if (!isProcessing)
            ExecutePlans();
        else if (makingAssignment)
            DoAssignment();
        else if(assignmentTimer < 0 && !makingAssignment)
        {
            makingAssignment = true;
 
            GameObject peasants = GameObject.Find("Peasants");
            WorkerLogic[] peasantChildren = peasants.GetComponentsInChildren<WorkerLogic>();
            for (int i = 0; i < peasantChildren.Length; ++i)
                peasantChildren[i].taskAssigned = false;

            DoPlan();
            assignmentTimer = resetTime;
        }
    }

    // this function will execute plans in the stack one at a time 
    void ExecutePlans()
    {
        // pop the plan off the stack
        while(currentPlans.Count > 0 && isProcessing == false)
        {
            BuildPlan plan = currentPlans.Pop();
            // duplicate, one for local function, other for render text to display current plan
            string objToBuild = plan.ObjectName;
            currentPlanName = objToBuild;
            currentPlan = plan;
            isProcessing = true;
        }
        if (currentPlans.Count == 0 && isProcessing == false)
            currentPlanName = "None";
    }

    // this function will execute one plan at a time
    void DoPlan()
    {
        int goldRequired = currentPlan.plan.goldRequired;
        int woodRequired = currentPlan.plan.woodRequired;

        // we can build the building 
        if(BlackBoard.gold >= goldRequired && BlackBoard.wood >= woodRequired)
        {
            bool foundPeasant = false;

            // execute the build plan
            GameObject peasants = GameObject.Find("Peasants");
            WorkerLogic[] peasantChildren = peasants.GetComponentsInChildren<WorkerLogic>();
            for (int j = 0; j < peasantChildren.Length; ++j)
            {
                // if they arent carrying anything
                if (peasantChildren[j].prevState == WorkerLogic.WORKER_STATE.WORKER_RUN)
                {
                    // set this guy to build 
                    peasantChildren[j].toBuild = true;
                    EventManager.WorkerBuild(currentPlan.plan, peasantChildren[j]);
                    foundPeasant = true;
                    break;
                }
            }
            if(foundPeasant)
            {
                // reset timer so no one gets assigned anything for the next plan immediately
                assignmentTimer = resetTime;

                // add the building to the buildings we currently have
                BlackBoard.AddBuilding(currentPlan.plan.enumValue);
                BlackBoard.gold -= currentPlan.plan.goldRequired;
                BlackBoard.wood -= currentPlan.plan.woodRequired;

                // start processing next plan 
                currentPlanName = "";
                isProcessing = false;
                makingAssignment = false;
                return; 
            }
        }

        // if there is no current plan just go 50/50 ish
        if(currentPlanName == "None")
        {
            int numWorkersAvailable = (BlackBoard.numGoldWorkers + BlackBoard.numWoodWorkers) - BlackBoard.numWorkersBuilding;
            goldWorkersToBeAssigned = (int)(Mathf.Ceil(numWorkersAvailable / 2));
            woodWorkersToBeAssigned = numWorkersAvailable - goldWorkersToBeAssigned;
        }
        // calculate the values necessary to assign tasks
        else
        {
            float woodTimeLeft = ((float)woodRequired - (float)BlackBoard.wood) / BlackBoard.currWoodRate;
            float goldTimeLeft = ((float)goldRequired - (float)BlackBoard.gold) / BlackBoard.currGoldRate;

            float woodPerWorker = BlackBoard.currWoodRate / BlackBoard.numWoodWorkers;
            float goldPerWorker = BlackBoard.currGoldRate / BlackBoard.numGoldWorkers;

            if (woodTimeLeft < 0)
                woodTimeLeft = 0;
            if (goldTimeLeft < 0)
                goldTimeLeft = 0;

            float diff = goldTimeLeft - woodTimeLeft;
            int newWoodWorkerCount = BlackBoard.numWoodWorkers;
            int newGoldWorkerCount = BlackBoard.numGoldWorkers;

            int numWorkersAvailable = (BlackBoard.numGoldWorkers + BlackBoard.numWoodWorkers) - BlackBoard.numWorkersBuilding;

            if (diff > 0) // more time left to complete gold than wood
            {
                for(int i = BlackBoard.numWoodWorkers; i > 0; --i) // try assigning more peasants to gold from wood
                {
                    float woodTimeLeftNew = ((float)woodRequired - (float)BlackBoard.wood) / (woodPerWorker * i);
                    float goldTimeLeftNew = ((float)goldRequired - (float)BlackBoard.gold) / (goldPerWorker *(numWorkersAvailable - i));

                    if ((numWorkersAvailable - i) == 0)
                    {
                        goldTimeLeftNew = 10000;
                    }
                    if(diff > Mathf.Abs(goldTimeLeftNew - woodTimeLeftNew))
                    {
                        diff = Mathf.Abs(goldTimeLeftNew - woodTimeLeftNew);
                        newWoodWorkerCount = i;
                        newGoldWorkerCount = numWorkersAvailable - i;
                    }

                }
            }
            else if (diff < 0)
            {
                for (int i = BlackBoard.numGoldWorkers; i > 0; --i) // try assigning more peasants to wood from gold
                {
                    float woodTimeLeftNew = ((float)woodRequired - (float)BlackBoard.wood) / (woodPerWorker * (numWorkersAvailable - i));
                    float goldTimeLeftNew = ((float)goldRequired - (float)BlackBoard.gold) / (i * goldPerWorker);

                    if ((numWorkersAvailable - i) == 0)
                    {
                        woodTimeLeftNew = 10000;
                    }

                    if (Mathf.Abs(diff) > Mathf.Abs(goldTimeLeftNew - woodTimeLeftNew))
                    {
                        diff = Mathf.Abs(goldTimeLeftNew - woodTimeLeftNew);
                        newWoodWorkerCount = (numWorkersAvailable -i);
                        newGoldWorkerCount = i;
                    }

                }
            }
            goldWorkersToBeAssigned = newGoldWorkerCount;
            woodWorkersToBeAssigned = newWoodWorkerCount;
            DoAssignment();
        }
    }

    void DoAssignment()
    {
        totalWorkersLeftToBeAssiged = goldWorkersToBeAssigned + woodWorkersToBeAssigned;

        // if we made all the assignments return
        if (totalWorkersLeftToBeAssiged == 0){
            makingAssignment = false;
            return;
        }

        GameObject peasants = GameObject.Find("Peasants");
        WorkerLogic[] peasantChildren = peasants.GetComponentsInChildren<WorkerLogic>();
        for (int j = 0; j < peasantChildren.Length; ++j)
        {
            // they have already been assigned a new task in the previous iteration
            if (peasantChildren[j].taskAssigned)
                continue;

            // if this peasant is currently getting gold 
            if(peasantChildren[j].state == WorkerLogic.WORKER_STATE.WORKER_BAG)
            {
                // if we need to make a gold assignment, let the peasant continue grabbing gold
                if(goldWorkersToBeAssigned > 0){
                    //Debug.Log("GOLD WORKERS TO BE ASSIGNED " + goldWorkersToBeAssigned);
                    goldWorkersToBeAssigned -= 1;
                    peasantChildren[j].taskAssigned = true;
                    continue;
                }
            }
            // if this peasant is currently getting wood 
            else if (peasantChildren[j].state == WorkerLogic.WORKER_STATE.WORKER_WOOD)
            {
                // if we need to make a wood assignment, let the peasant continue grabbing wood
                if (woodWorkersToBeAssigned > 0){
                    //Debug.Log("WOOD WORKERS TO BE ASSIGNED " + woodWorkersToBeAssigned);
                    woodWorkersToBeAssigned -= 1;
                    peasantChildren[j].taskAssigned = true;
                    continue;
                }
            }
                
            // we need to assign someone to gold and there are no peasants left currently grabbing gold
            if(goldWorkersToBeAssigned > 0)
            {
                // if they are currently in the state of grabbing wood
                if (peasantChildren[j].state == WorkerLogic.WORKER_STATE.WORKER_WOOD)
                {
                    // reassign them to carry gold
                    AssignWorker(peasantChildren[j], WorkerLogic.WORKER_STATE.WORKER_BAG);
                    goldWorkersToBeAssigned -= 1;

                    peasantChildren[j].anim.SetBool("CarryWood", false); // safety net
                    peasantChildren[j].pathfinding = false;
                    continue;
                }
            }
            // We need to assign someone to wood and there are no peasants left currently grabbing wood
            if (woodWorkersToBeAssigned > 0)
            {
                // if they are currently in the state of grabbing gold
                if (peasantChildren[j].state == WorkerLogic.WORKER_STATE.WORKER_BAG)
                {
                    // reassign them to carry gold
                    AssignWorker(peasantChildren[j], WorkerLogic.WORKER_STATE.WORKER_WOOD);
                    woodWorkersToBeAssigned -= 1;

                    peasantChildren[j].anim.SetBool("CarryGold", false); // safety net
                    peasantChildren[j].pathfinding = false;
                    continue;
                }
            }
        }
        // if all the assignments were made return
        if (totalWorkersLeftToBeAssiged == 0){
            makingAssignment = false;
            return;
        }
    }
     
    void AssignWorker(WorkerLogic logic, WorkerLogic.WORKER_STATE newState)
    {
        logic.state = newState;
        logic.taskAssigned = true;
    }
}