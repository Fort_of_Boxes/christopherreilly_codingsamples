﻿using System.Collections;
using UnityEngine;

public class Unit : MonoBehaviour {
    
    public Vector3 target;
    public float speed = 12;
    public float turnDist = 5;
    public float turnSpeed = 3;
    const float minPathUpdateTime       =.2f;
    const float pathUpdateMoveThreshold = .5f; // if target has moved more than this recompute path

    private bool  storePos = true;
    private float timer = 5.0f;
    private float stuckSpeed = 5.0f;

    Grid grid;

    Vector3 prevPos;
    Vector3 currentPos;
    
    Path path;

    [HideInInspector]
    public bool pathComplete = false;

    void OnEnable()
    {
        EventManager.OnUpdateGrid += RepathToTarget;
    }

    void OnDisable()
    {
        EventManager.OnUpdateGrid -= RepathToTarget;
    }
    void Start()
    {
        StartCoroutine(UpdatePath());
        grid = GameObject.Find("A*").GetComponent<Grid>();
    }

    void RepathToTarget()
    {
        StartPath(transform.position, target);
    }

    bool CheckStuck()
    {
        // get the grid node position 
        Node nodePosCurr = grid.NodeFromWorldPos(currentPos);
        Node nodePosPrev = grid.NodeFromWorldPos(prevPos);

        if (nodePosCurr == nodePosPrev)
        {
            if(GetComponent<WorkerLogic>().state == WorkerLogic.WORKER_STATE.WORKER_WOOD)
                GetComponent<WorkerLogic>().state = WorkerLogic.WORKER_STATE.WORKER_BAG;
            else
                GetComponent<WorkerLogic>().state = WorkerLogic.WORKER_STATE.WORKER_WOOD;

            storePos = true;
            return true;
        }
        else
            storePos = true;
            return false;
    }

    void Update()
    {
        timer -= Time.deltaTime * stuckSpeed;
        if(storePos)
        {
            prevPos = transform.position;
            timer = 5.0f;
            storePos = false;
        }
        if(timer < 0)
        {
            CheckStuck();
            timer = 0.0f;
        }
        currentPos = transform.position;
        speed = BlackBoard.workerSpeed;
    }

    public void StartPath(Vector3 position, Vector3 goal)
    {
        pathComplete = false;
        target = goal; // make sure to store target in case we need to recalc
        PathRequestManager.RequestPath(position, goal, OnPathFound);
    }

    public void OnPathFound(Vector3[] waypoints, bool pathSuccess)
    {
        if(pathSuccess)
        {
            path = new Path(waypoints, transform.position, turnDist);
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }

    IEnumerator UpdatePath()
    {
        if (Time.timeSinceLevelLoad < .3f)
            yield return new WaitForSeconds(.3f);
        float sqrMoveThreshold = pathUpdateMoveThreshold * pathUpdateMoveThreshold;
        Vector3 targetPosOld = target;

        while (true)
        {
            yield return new WaitForSeconds(minPathUpdateTime);
            if((target - targetPosOld).sqrMagnitude > sqrMoveThreshold)
            PathRequestManager.RequestPath(transform.position, target, OnPathFound);
            targetPosOld = target;
        }
    }

     IEnumerator FollowPath()
    {
        bool followingPath = true;
        int pathIndex = 0;
        if(path.lookPoints.Length == 0)
        {
            followingPath = false;
            pathComplete = true;
            yield return null;
        }
        transform.LookAt(path.lookPoints[0]); // Look where walking
        while (followingPath)
        {
            Vector2 pos2D = new Vector2(transform.position.x, transform.position.z);
            while(path.turnBoundaries[pathIndex].HasCrossedLine(pos2D))
            {
                if(pathIndex > path.finishLineIndex)
                {
                    followingPath = false;
                    pathComplete = true;
                    break;
                }

                if (pathIndex == path.finishLineIndex)
                {
                    followingPath = false;
                    pathComplete = true;
                    break;
                }
                else
                    pathIndex++;
            }
            if(followingPath) // rotate and move unit
            {
                Quaternion targetRot = Quaternion.LookRotation(path.lookPoints[pathIndex] - transform.position);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, Time.deltaTime * turnSpeed);
                transform.Translate(Vector3.forward * Time.deltaTime * speed, Space.Self);
                if (Vector3.Distance(transform.position, target) < 5.0)
                {
                    followingPath = false;
                    pathComplete = true;
                    break;
                }

            }
            yield return null;
        }
    }

    public void OnDrawGizmos() // Draw Path
    {
        if(path != null)
            path.DrawWithGizmos();   
    }
}
