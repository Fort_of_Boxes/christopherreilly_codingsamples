﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerLogic : MonoBehaviour
{
    [HideInInspector]
    public enum WORKER_STATE
    {
        WORKER_RUN,
        WORKER_BAG,
        WORKER_WOOD,
        WORKER_BUILD
    }

    // public variables
    public  WORKER_STATE state     = WORKER_STATE.WORKER_RUN;

    // private variables
    [HideInInspector]
    public  WORKER_STATE prevState = WORKER_STATE.WORKER_RUN;
    private GameObject  goldMine;
    private static GameObject  townHall;
    private static GameObject  lumberMill;
    private FindNearestObjectByTag findObjectNearest;
    private Unit unit;

    public Animator anim;
    public bool pathfinding = false;
    public bool taskAssigned;
    public bool toBuild = false;

    public GameObject buildManager;
    public Vector3   buildLocation;           // where the unit moves to 
    public bool      buildPathFound = false;
    private string   buildingToBuild;         // which building to build
    private Vector3  buildingBuildLocation;   // where the building will be build
    private FindBuildLocation fBuildLocation;


    void Start()
    {
        goldMine          = GameObject.Find("MineCenter");
        townHall          = GameObject.Find("TownHall");
        lumberMill        = GameObject.Find("TownHall");
        buildManager      = GameObject.Find("BuildManager");
        findObjectNearest = GameObject.Find("A*").GetComponent<FindNearestObjectByTag>();
        anim              = GetComponent<Animator>();
        unit              = GetComponent<Unit>();
        taskAssigned      = true;
        fBuildLocation    = buildManager.GetComponent<FindBuildLocation>();
    }

    void OnEnable()
    {
        EventManager.OnLocationFound += BuildLocationFound;
    }

    void OnDisable()
    {
        EventManager.OnLocationFound -= BuildLocationFound;
    }

    void CompletePath(WORKER_STATE newState)
    {
        prevState = newState;
        pathfinding = false;
    }

    void GatherWood()
    {
        if(prevState == WORKER_STATE.WORKER_RUN)
        {
            // move towards the closest tree 
            if(!pathfinding){
                anim.SetBool("CarryWood", false);
                GameObject tree = FindNearestObjectByTag.GetNearestObjectByTag(transform.position, "Tree");
                unit.StartPath(transform.position, tree.GetComponent<Transform>().position);
                pathfinding = true;
            }
            if (unit.pathComplete)
            {
                if(toBuild == true){
                    pathfinding = false;
                    state = WORKER_STATE.WORKER_BUILD;
                }
                else
                    CompletePath(state);
            }
        }
        else if(prevState == state)
        {
            // return the wood to the town hall or lumberyard if one exists
            if (!pathfinding){
                anim.SetBool("CarryWood", true);

                Vector3 targetPos;

                // find the closest lumber mill
                GameObject lumber = FindNearestObjectByTag.GetNearestObjectByTag(transform.position, "LumberMill");
                if (lumber != null)
                    lumberMill = lumber;
                else // no lumberMill exists, go to town hall
                    lumberMill = townHall;

                targetPos = fBuildLocation.GetNearestPointToBuilding(transform.position, lumberMill.GetComponent<Transform>().position, lumberMill.GetComponent<SphereCollider>().radius, 0);
                unit.StartPath(transform.position, targetPos);
                pathfinding = true;
            }
            if (unit.pathComplete)
            {
                BlackBoard.wood += 10;
                if (toBuild == true){
                    pathfinding = false;
                    state = WORKER_STATE.WORKER_BUILD;
                }
                else
                    CompletePath(WORKER_STATE.WORKER_RUN);
            }
        }
    }

    void GatherGold()
    {
        if (prevState == WORKER_STATE.WORKER_RUN)
        {
            // move towards the gold mine
            if (!pathfinding){ 
                anim.SetBool("CarryGold", false);
                unit.StartPath(transform.position, goldMine.GetComponent<Transform>().position);
                pathfinding = true;
            }
            if (unit.pathComplete)
            {
                if (toBuild == true)
                {
                    pathfinding = false;
                    state = WORKER_STATE.WORKER_BUILD;
                }
                else
                    CompletePath(state);
            }
        }
        else if (prevState == state)
        {
            // move towards the town hall to return the gold
            if (!pathfinding){
                anim.SetBool("CarryGold", true);
                Vector3 targetPos = fBuildLocation.GetNearestPointToBuilding(transform.position, townHall.GetComponent<Transform>().position, townHall.GetComponent<SphereCollider>().radius, 0);
                unit.StartPath(transform.position, targetPos);
                pathfinding = true;
            }
            if (unit.pathComplete)
            {
                BlackBoard.gold += 10;
                if (toBuild == true)
                {
                    pathfinding = false;
                    state = WORKER_STATE.WORKER_BUILD;
                }
                else
                    CompletePath(WORKER_STATE.WORKER_RUN);
            }
        }
    }

    void BuildLocationFound(Vector3 location, Plan plan)
    {
        buildingToBuild       = plan.name;
        buildingBuildLocation = location;
    }

    void BuildBuilding()
    {
        prevState = WORKER_STATE.WORKER_RUN;
        anim.SetBool("CarryGold", false);
        anim.SetBool("CarryWood", false);

        if (!pathfinding && buildPathFound)
        {
            // start path to build location
            unit.StartPath(transform.position, buildLocation);
            pathfinding = true;
        }
        if (unit.pathComplete)
        {
            // stop movement, they are now building 
            GameObject obj = Instantiate(Resources.Load(buildingToBuild, typeof(GameObject))) as GameObject;
            obj.transform.position = buildingBuildLocation;

            // assign state 
            if(BlackBoard.numGoldWorkers > BlackBoard.numWoodWorkers)
                state = WORKER_STATE.WORKER_WOOD;
            else
                state = WORKER_STATE.WORKER_BAG;


            EventManager.StartBuilding();


            toBuild = false;
            buildPathFound = false;
            pathfinding = false;
        }
    }
	
	void Update ()
    {
        switch (state)
        {
            case WORKER_STATE.WORKER_WOOD:
                GatherWood();
                break;

            case WORKER_STATE.WORKER_BAG:
                GatherGold();
                break;

            case WORKER_STATE.WORKER_BUILD:
                BuildBuilding();
                break;
        }
	}
}
